package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestGetAutosFromMake {

	@Test
	void testNullMake() {
		Inventory inventory = new Inventory();
		assertThrows(IllegalArgumentException.class, () -> inventory.getAutosFromMake(null));
	}
	
	@Test
	void testEmptyMake() {
		Inventory inventory = new Inventory();
		assertThrows(IllegalArgumentException.class, () -> inventory.getAutosFromMake(""));
	}
	
	@Test
	void testEmptyInventory() {
		Inventory inventory = new Inventory();
		
		ArrayList<Automobile> result = inventory.getAutosFromMake("Jeep");
		
		assertEquals(0, result.size());
	}
	
	@Test
	void testMatchFoundInBeginning() {
		Inventory inventory = new Inventory();
		
		inventory.add(new Automobile("Jeep", "Wrangler", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Explorer", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "Silverado", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Tesla", "Model S", 2008, 108132.2, 6900));
		
		ArrayList<Automobile> result = inventory.getAutosFromMake("Jeep");
		
		assertEquals(1, result.size());
		assertEquals("Jeep", result.get(0).getMake());
	}
	
	@Test
	void testMatchFoundInEnd() {
		Inventory inventory = new Inventory();
		
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Explorer", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "Silverado", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Tesla", "Model S", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Jeep", "Wrangler", 2008, 108132.2, 6900));
		
		ArrayList<Automobile> result = inventory.getAutosFromMake("Jeep");
		
		assertEquals(1, result.size());
		assertEquals("Jeep", result.get(0).getMake());
	}
	
	@Test
	void testMatchFoundInMiddle() {
		Inventory inventory = new Inventory();
		
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Explorer", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Jeep", "Wrangler", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "Silverado", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Tesla", "Model S", 2008, 108132.2, 6900));
		
		ArrayList<Automobile> result = inventory.getAutosFromMake("Jeep");
		
		assertEquals(1, result.size());
		assertEquals("Jeep", result.get(0).getMake());
	}
	
	@Test
	void testMultipleMatches() {
		Inventory inventory = new Inventory();
		
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Jeep", "Cherokee", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Jeep", "Wrangler", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "Silverado", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Jeep", "Patriot", 2008, 108132.2, 6900));
		
		ArrayList<Automobile> result = inventory.getAutosFromMake("Jeep");
		
		assertEquals(3, result.size());
	}
	
	@Test
	void testNoMatches() {
		Inventory inventory = new Inventory();
		
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Explorer", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Tesla", "Model X", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Chevy", "Silverado", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Tesla", "Model S", 2008, 108132.2, 6900));
		
		ArrayList<Automobile> result = inventory.getAutosFromMake("Jeep");
		
		assertEquals(0, result.size());
	}

}
