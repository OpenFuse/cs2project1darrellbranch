package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestAdd {

	@Test
	void testAddNullAuto() {
		Inventory inventory = new Inventory();
		assertThrows(IllegalArgumentException.class, () -> inventory.add(null));
	}
	
	@Test
	void testAddAutoToEmptyInventory() {
		Inventory inventory = new Inventory();
		boolean added = inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		assertEquals(1, inventory.size());
		assertTrue(added);
	}
	
	@Test
	void testAddMultiplesAutoToInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		boolean added = inventory.add(new Automobile("Ford", "Explorer", 2008, 108132.2, 6900));
		assertEquals(2, inventory.size());
		assertTrue(added);
	}
	
	@Test
	void testAddMultipleIdenticleAutosToInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		boolean added = inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		assertEquals(1, inventory.size());
		assertFalse(added);
	}

}
