package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.Inventory;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {
	
	private NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param dealer The dealership to build summary report for
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Dealership dealer) {
		String summary = "";
		
		Inventory inventory = dealer.getInventory();

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = dealer.getName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			summary += System.lineSeparator();
			summary += this.buildMostAndLeastExpensiveSummaryOutput(inventory);
			
			summary += System.lineSeparator();
			summary += this.buildAverageMilesReport(inventory);
			
			summary += System.lineSeparator();
			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(3000, inventory);

			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(10000, inventory);
		}

		return summary;
	}

	private String buildAverageMilesReport(Inventory inventory) {
		String report = "Average miles: ";
		double averageMiles = inventory.computeAverageMiles();
		
		DecimalFormat milesFormat = new DecimalFormat("#,###.000");
		report += milesFormat.format(averageMiles);
		
		return report;
	}

	private String buildMostAndLeastExpensiveSummaryOutput(Inventory inventory) {
		Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
		Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();

		String report = "Most expensive auto: ";
		report += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();
		
		report += "Least expensive auto: ";
		report += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();
		
		return report;
	}

	private String buildPriceSegmentBreakdownSummary(double priceSegmentRange, Inventory inventory) {
		int[] autoPriceSegmentsCount = inventory.countVehiclesInEachPriceSegment(priceSegmentRange);
		
		if (autoPriceSegmentsCount == null) {
			return "";
		}

		String priceSegmentSummary = "Vehicles in " + this.currencyFormatter.format(priceSegmentRange) + " segments" + System.lineSeparator();
		
		double startingPrice = 0;
		double endingPrice = priceSegmentRange;
		
		for (int i = 0; i < autoPriceSegmentsCount.length; i++) {
			String startingPriceCurrencyFormat = this.currencyFormatter.format(startingPrice);
			String endingPriceCurrencyFormat = this.currencyFormatter.format(endingPrice);
			priceSegmentSummary += startingPriceCurrencyFormat + " - " + endingPriceCurrencyFormat + " : ";
			
			if (autoPriceSegmentsCount[i] == 0) {
				priceSegmentSummary += "0" + System.lineSeparator();
			} else {
				
				for (int j = 0; j < autoPriceSegmentsCount[i]; j++) {
					priceSegmentSummary += "*";
				}
				
				priceSegmentSummary += System.lineSeparator();
				
			}
			
			startingPrice = endingPrice + 0.01;
			endingPrice = (priceSegmentRange * (i + 2)); 
		}
		
		return priceSegmentSummary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		String output = this.currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake() + " "
				+ auto.getModel();
		return output;
	}
	
	/**
	 * Gets the auto report by make.
	 *
	 * @precondition make not null or empty; dealer not null
	 * @postcondition none
	 * @param make the make
	 * @param dealer the dealer
	 * @return the auto report by make
	 */
	public String getAutoReportByMake(String make, Dealership dealer) {
		
		if (make == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MAKE_CANNOT_BE_NULL);
		}
		if (make.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MAKE_CANNOT_BE_EMPTY);
		}
		if (dealer == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERS_CANNOT_BE_NULL);
		}
		String output = "";
		ArrayList<Automobile> autosFound = dealer.getInventory().getAutosFromMake(make);
		
		if (autosFound.size() == 0) {
			output = "No " + make + "s at " + dealer.getName();
			return output;
		}
		output = "Autos by " + make + " at " + dealer.getName() + System.lineSeparator();
		Double sumOfPrices = 0.00;
		for (Automobile currAuto: autosFound) {
			output += currAuto.getModel() + " " + currAuto.getYear() + " " + currAuto.getMiles() + " " + currAuto.getPrice() + System.lineSeparator();
			sumOfPrices += currAuto.getPrice();
		}
		Double average = sumOfPrices / autosFound.size();
		NumberFormat currencyFormatter =
			    NumberFormat.getCurrencyInstance(Locale.US);
		
		output += "Average Price: " + currencyFormatter.format(average) + System.lineSeparator();
		
		return output;
	}

}
