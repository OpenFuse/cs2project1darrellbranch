package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class Inventory.
 * 
 * @author CS1302
 */
public class Inventory {
	
	/** The Constant START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR. */
	private static final String START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR = "startYear must be <= endYear.";
	
	/** The Constant AUTO_CANNOT_BE_NULL. */
	private static final String AUTO_CANNOT_BE_NULL = "auto cannot be null.";

	/** The autos. */
	private ArrayList<Automobile> autos;

	/**
	 * Instantiates a new inventory.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public Inventory() {
		this.autos = new ArrayList<Automobile>();
	}

	/**
	 * Numbers of autos in inventory.
	 *
	 * @return the number of autos in the inventory.
	 * @precondition none
	 * @postcondition none
	 */
	public int size() {
		return this.autos.size();
	}

	/**
	 * Adds the auto to the inventory.
	 *
	 * @param auto the auto
	 * @return true, if add successful, false otherwise
	 * @precondition auto != null
	 * @postcondition size() == size()@prev + 1
	 */
	public boolean add(Automobile auto) {
		if (auto == null) {
			throw new IllegalArgumentException(AUTO_CANNOT_BE_NULL);
		}
		
		if (this.autos.size() > 0) {
			
			for (Automobile currAuto: this.autos) {
				if (currAuto.getMake() == auto.getMake() && currAuto.getModel() == auto.getModel() && currAuto.getYear() == currAuto.getYear()) {
					return false;
				}
			}
			
			return this.autos.add(auto);
			
		}
		
		return this.autos.add(auto);
		
	}

	/**
	 * Adds all the autos to the inventory.
	 *
	 * @param autos the autos to add to the inventory
	 * @return true, if add successful
	 * @precondition autos != null
	 * @postcondition size() == size()@prev + autos.size()
	 */
	public boolean addAll(ArrayList<Automobile> autos) {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		return this.autos.addAll(autos);
	}

	/**
	 * Creates an array that holds the count of the number of automobiles in each
	 * price segment starting from 0 to priceSementRange up to the last range which
	 * includes the maximum price car.
	 *
	 * @param priceSegmentRange the price range
	 * @return Array with number of autos in inventory that are in each price
	 *         segment. Returns null if no autos in inventory.
	 * @precondition none
	 * @postcondition none.
	 */
	public int[] countVehiclesInEachPriceSegment(double priceSegmentRange) {

		if (this.size() == 0) {
			return null;
		}

		double maxPrice = this.findMostExpensiveAuto().getPrice();
		int segments = (int) Math.ceil(maxPrice / priceSegmentRange);
		int[] vehicleCount = new int[segments];

		for (Automobile currAuto : this.autos) {
			double currPrice = currAuto.getPrice() - 0.0001;
			int segmentNumber = (int) (currPrice / priceSegmentRange);
			vehicleCount[segmentNumber]++;
		}

		return vehicleCount;
	}

	/**
	 * Compute average miles of automobiles in inventory.
	 *
	 * @return average miles; if size() == 0 returns 0
	 * @precondition none
	 * @postcondition none
	 */
	public double computeAverageMiles() {
		double sum = 0;

		if (this.autos.size() == 0) {
			return 0;
		}

		for (Automobile currAuto : this.autos) {
			sum += currAuto.getMiles();
		}

		double average = sum / this.autos.size();
		return average;
	}

	/**
	 * Find year of oldest auto.
	 *
	 * @return the year of the oldest auto or Integer.MAX_VALUE if no autos
	 * @precondition none
	 * @postcondition none
	 */
	public int findYearOfOldestAuto() {
		int oldest = Integer.MAX_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() < oldest) {
				oldest = automobile.getYear();
			}
		}

		return oldest;
	}

	/**
	 * Find year of newest auto.
	 *
	 * @return the year of the newest auto or Integer.MIN_VALUE if no autos
	 * @precondition none
	 * @postcondition none
	 */
	public int findYearOfNewestAuto() {
		int newest = Integer.MIN_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() > newest) {
				newest = automobile.getYear();
			}
		}

		return newest;
	}

	/**
	 * Find most expensive auto in the inventory.
	 *
	 * @return the most expensive automobile or null if no autos in inventory.
	 * @precondition none
	 * @postcondition none
	 */
	public Automobile findMostExpensiveAuto() {
		Automobile maxAuto = null;
		double maxPrice = 0;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() > maxPrice) {
				maxAuto = currAuto;
				maxPrice = maxAuto.getPrice();
			}
		}

		return maxAuto;
	}

	/**
	 * Find least expensive auto in the inventory.
	 *
	 * @return the least expensive automobile or null if no autos in inventory.
	 * @precondition none
	 * @postcondition none
	 */
	public Automobile findLeastExpensiveAuto() {
		Automobile minAuto = null;
		double minPrice = Double.MAX_VALUE;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() < minPrice) {
				minAuto = currAuto;
				minPrice = minAuto.getPrice();
			}
		}

		return minAuto;
	}

	/**
	 * Count autos between specified start and end year inclusive.
	 *
	 * @param startYear the start year
	 * @param endYear   the end year
	 * @return the number of autos between start and end year inclusive.
	 * @precondition startYear <= endYear
	 * @postcondition none
	 */
	public int countAutosBetween(int startYear, int endYear) {
		if (startYear > endYear) {
			throw new IllegalArgumentException(START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR);
		}
		int count = 0;
		for (Automobile automobile : this.autos) {
			if (automobile.getYear() >= startYear && automobile.getYear() <= endYear) {
				count++;
			}
		}

		return count;
	}
	
	
	/**
	 * Gets the autos from make.
	 *
	 *@precondition make not null not empty
	 *@postcondition none
	 * @param make the make
	 * @return the autos from make
	 */
	public ArrayList<Automobile> getAutosFromMake(String make) {
		
		if (make == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MAKE_CANNOT_BE_NULL);
		}
		if (make.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.MAKE_CANNOT_BE_EMPTY);
		}
		if (this.autos.size() == 0) {
			return new ArrayList<Automobile>();
		}
		
		String normalizedMake = make.toLowerCase();
		ArrayList<Automobile> autosFound = new ArrayList<Automobile>();
		
		for (Automobile currAuto: this.autos) {	
			if (currAuto.getMake().toLowerCase().equals(normalizedMake)) {
				autosFound.add(currAuto);
			}
		}
		
		return autosFound;
	}

	/**
	 * Deletes the specified auto from the inventory.
	 *
	 * @param auto The automobile to delete from the inventory.
	 * @return true if the auto was found and deleted from the inventory, false
	 *         otherwise
	 * @precondition none
	 * @postcondition if found, size() == size()@prev � 1
	 */
	public boolean remove(Automobile auto) {
		return this.autos.remove(auto);
	}

	/**
	 * Gets the autos.
	 *
	 * @return the autos
	 * @precondition none
	 * @postcondition none
	 */
	public ArrayList<Automobile> getAutos() {
		return this.autos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "#autos:" + this.size();
	}

}
