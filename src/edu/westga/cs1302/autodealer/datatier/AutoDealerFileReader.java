package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AutoDealerFileReader. Reads an .adi (Auto Dealer Inventory) file which is a
 * CSV file with the following format:
 * make,model,year,miles,price
 * 
 * @author CS1302
 */
public class AutoDealerFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File inventoryFile;

	/**
	 * Instantiates a new auto dealer file reader.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile
	 *            the inventory file
	 */
	public AutoDealerFileReader(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}
		
		this.inventoryFile = inventoryFile;
	}

	/**
	 * Opens the associated adi file and reads all the autos in the file one
	 * line at a time. Parses each line and creates an automobile object and stores it in
	 * an ArrayList of Automobile objects. Once the file has been completely read the
	 * ArrayList of Automobiles is returned from the method. Assumes all
	 * autos in the file are for the same dealership.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Automobile objects read from the file.
	 * @throws FileNotFoundException File not found. 
	 */
	public ArrayList<Automobile> loadAllAutos() throws FileNotFoundException {
		ArrayList<Automobile> autos = new ArrayList<Automobile>();
		
		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);
				
				try {
					Automobile auto = this.makeAutomobile(fields);
					autos.add(auto);
				} catch (Exception e) {
					String error = " Error reading file: ";
					error += e.getMessage() + " :  " + line + System.lineSeparator();
					System.err.print(error);
				}
			}
		}

		return autos;  
	}
	
	/**
	 * Load dealerships and populates them with autos.
	 *
	 * @precondition none
	 * @postcondition none
	 * @param dealershipGroup the dealership group
	 * @throws FileNotFoundException the file not found exception
	 */
	public void loadDealerships(DealershipGroup dealershipGroup) throws FileNotFoundException {
		try (Scanner input = new Scanner(this.inventoryFile)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.splitLine(line, AutoDealerFileReader.FIELD_SEPARATOR);
				
				String[] autoFields = {fields[1], fields[2], fields[3], fields[4], fields[5]};
				String dealerName = fields[0];
				
				try {
					Dealership dealer = dealershipGroup.findDealership(dealerName);
					Dealership defaultDealer = dealershipGroup.findDealership("MY AUTOS");
					Automobile newAuto = this.makeAutomobile(autoFields);
					
					if (dealer == null) {
						dealershipGroup.addDealership(dealerName);
						dealer = dealershipGroup.findDealership(dealerName);
						newAuto.setDealer(dealer.getName());
						dealer.getInventory().add(newAuto);
					}	
					newAuto.setDealer(dealer.getName());
					dealer.getInventory().add(newAuto);
					defaultDealer.getInventory().add(newAuto);
					
					
				} catch (Exception e) {
					
					String error = "Error reading file: ";
					error += e + " : " + line + System.lineSeparator();
					System.err.print(error);

				}
			}
		}
	}

	private Automobile makeAutomobile(String[] fields) {
		String make = fields[0];
		String model = fields[1];
		int year = Integer.parseInt(fields[2]);
		double miles = Double.parseDouble(fields[3]);
		double price = Double.parseDouble(fields[4]);
		return new Automobile(make, model, year, miles, price);
	}

	private String[] splitLine(String line, String fieldSeparator) {
		return line.split(fieldSeparator);
	}

}
