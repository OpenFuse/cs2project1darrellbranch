package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AlbumFileWriter.
 * 
 * @author CS1302
 */
public class AutoDealerFileWriter {

	private File inventoryFile;

	/**
	 * Instantiates a new auto file writer.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile the inventory file
	 */
	public AutoDealerFileWriter(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}

		this.inventoryFile = inventoryFile;
	}

	/**
	 * Writes all the autos in the dealership to the specified auto file. Each auto
	 * will be on a separate line and of the following format:
	 * make,model,year,miles,price
	 * 
	 * @precondition autos != null
	 * @postcondition none
	 * 
	 * @param autos The collection of autos to write to file.
	 * @throws FileNotFoundException 
	 */
	public void write(ArrayList<Automobile> autos) throws FileNotFoundException {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Automobile currAuto : autos) {
				String output = currAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getPrice();
				
				writer.println(output);
			}
		}

	}
	
	/**
	 * Save autos with dealership.
	 *
	 * @precondition dealershipGroup != null
	 * @postcondition none
	 * @param dealershipGroup the dealership group
	 * @throws FileNotFoundException the file not found exception
	 */
	public void writeAutosWithDealership(DealershipGroup dealershipGroup) throws FileNotFoundException {
		if (dealershipGroup == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.DEALERS_CANNOT_BE_NULL);
		}
		
		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Dealership currDealer: dealershipGroup.getDealers()) {
				
				String output = "";
				
				if (!currDealer.getName().equals("MY AUTOS")) {
					for (Automobile currAuto: currDealer.getInventory().getAutos()) {
						output += currDealer.getName() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currAuto.getPrice() + System.lineSeparator();
					}	
				}
				
				writer.print(output);
			}
		}
	}
	
	
	/**
	 * Appends auto details to log
	 *
	 * @precondition auto not null
	 * @postcondition none
	 * @param auto the auto
	 * @throws FileNotFoundException the file not found exception
	 */
	public void appendAutoDetails(Automobile auto) throws FileNotFoundException {
		if (auto == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTO_CANNOT_BE_NULL);
		}
		
		try (PrintWriter writer = new PrintWriter(new FileOutputStream(this.inventoryFile, true))) {
			NumberFormat currencyFormatter =
				    NumberFormat.getCurrencyInstance(Locale.US);
			
			String output = auto.getMake() + " ";
			output += auto.getModel() + " ";
			output += auto.getYear() + " ";
			output += auto.getMiles() + "mi ";
			output += currencyFormatter.format(auto.getPrice()) + " ";
			output += "sold by " + auto.getDealer();
			
			writer.println(output);
			
		}
	}

}
